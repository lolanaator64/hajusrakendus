<?php

namespace App\Http\Controllers;

use App\Models\Neko;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class NekoController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {

        return Inertia::render('Neko/Index', [
            'games' => Neko::all(),
            'images' => Media::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'title' => 'required|string|max:255',
            'description' => 'required|string|max:500',
            'playtime' => 'required|string|max:255',
            'rating' => 'required|string|max:255',
        ]);
        $neko = Neko::create($validated);
        if ($images = $request->file('images')) {
            foreach ($images as $image) {
                $neko->addMedia($image)->toMediaCollection('images');
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Neko $neko)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Neko $neko)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Neko $neko)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Neko $neko)
    {
        //
    }
}
